# Speaker Analyzer

Speaker Analyzer is an open source Matlab program to analyse and dimension loudspeaker systems. Loudspeaker systems with closed and bassreflex enclosures and the model of a speaker in infinite baffle are supported. The sound pressure level of loudspeakers are calculated directly from the Thiele-Small parameters of a speaker and relates to the sound characteristics of the loudspeaker system. By specifying the sound characteristics the parameters of the enclosures are calculated automatically. Therefore the program is an ideal tool to optimize loudspeakers in all aspects to the desired sound characteristics.

The program is based on the Bachelor’s Thesis [Elektroakustische Modellbildung und Optimierung von Lautsprechersystemen](https://loackerschoech.at/publication/bachelorthesis/) and the Master Project [Realisierung eines modularen Lautsprechersystems](https://loackerschoech.at/publication/masterproject/).

## Getting Started

* Start Matlab on your system
* Open Speaker_Analyzer.m
* Run Speaker_Analyzer

### Prerequisites

* Matlab R2015a or newer

## License

This project is licensed under the GNU General Public License - see the [LICENSE](LICENSE) file for details

## Author

* Florian Loacker-Schöch
