%--------------------------------------------------------------------------
% Speaker_Analyzer.m - Program to analyse and dimension loudspeaker systems
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function Speaker_Analyzer

    close;
    path('functions',path);

    % general settings
    font_size = 10;
            
    % program structure
    param = add_param;                  % add model paramters
    window = add_components(param);     % add gui components
    gui_init();
    
    %% generate gui
 
    % gui init 
    function gui_init()
        
        window.figure.Visible = 'on';   % make figure visible after adding components
        recalc_param;                   % calculate init values
        update_ui;                      % update text inputs    
        plot_diagrams;                  % update diagrams
        
        % calculate filter coeff
        param.gG.a1 = Bessel_coeff_calc_gG();
        [param.vG.a1,param.vG.a2,param.vG.a3] = Bessel_coeff_calc_vG();
            
    end

    % add components 
    function obj = add_components(param)
        
            obj.figure = figure('Name','Speaker Analyzer',...
                        'NumberTitle','off','Visible','off','Units','pixels',...
                        'Position',[100 100 1400 495],'SizeChangedFcn',@resize_ui);
                    
            obj.driver_param = add_driver_parameters(obj.figure,param);
            obj.max_values = add_max_values(obj.figure,param);
            obj.electr_model_param = add_electr_model_parameters(obj.figure,param);
            obj.model_settings = add_model_settings(obj.figure,param);
            obj.mech_model_param = add_mech_model_parameters(obj.figure);
            obj.akust_model_param = add_akust_model_parameters(obj.figure);
            
            obj.tabs = add_diagram_tabs(obj.figure);
    end

    % add driver thiele small parameter
    function obj = add_driver_parameters(fig,param)
        
            % settings
            y_offs = 135;
            y_diff = 25;          
            
            obj.panel = uipanel(fig,'Title','Basis Thiele Small Parameter','FontSize',font_size,...
                        'Units','pixels');
                    
            obj.input_Rs = input_field(obj.panel,'on','Rs',param.Rs,sprintf('[\x3a9]'),@Rs_changed,10,y_offs,60,130,180);
            obj.input_fuS = input_field(obj.panel,'on','fuS',param.fuS,'[Hz]',@fuS_changed,10,y_offs-1*y_diff,60,130,180);
            obj.input_QmMk = input_field(obj.panel,'on','Qm,Mk',param.QmMk,'[ ]',@QmMk_changed,10,y_offs-2*y_diff,60,130,180);
            obj.input_QeMk = input_field(obj.panel,'on','Qe,Mk',param.QeMk,'[ ]',@QeMk_changed,10,y_offs-3*y_diff,60,130,180);
            obj.input_Am = input_field(obj.panel,'on','Am',param.Am*1e4,sprintf('[cm\xb2]'),@Am_changed,10,y_offs-4*y_diff,60,130,190);
            obj.input_VaeMa = input_field(obj.panel,'on',sprintf('V\xe4,Ma'),param.VaeMa*1e3,'[l]',@VaeMa_changed,10,y_offs-5*y_diff,60,130,180);

    end

    % add maximal values
    function obj = add_max_values(fig,param)
        
            % settings
            y_offs = 35;
            y_diff = 25;          
            
            obj.panel = uipanel(fig,'Title','Maximalwerte','FontSize',font_size,...
                        'Units','pixels');
                    
            obj.input_xmax = input_field(obj.panel,'on','x,max',param.xmax*1e3,'[mm]',@xmax_changed,10,y_offs,60,130,180);
            obj.input_pmax = input_field(obj.panel,'on','pa,max',param.pmax,'[dB] in 1m',@pmax_changed,10,y_offs-1*y_diff,60,130,220);
    end

    % add electrical model parameters
    function obj = add_electr_model_parameters(fig,param)
        
            % settings
            y_offs = 65;
            y_diff = 25;          
            
            obj.panel = uipanel(fig,'Title','Elektrische Parameter','FontSize',font_size,...
                        'Units','pixels');
            
            obj.input_Ug = input_field(obj.panel,'on','Ug',param.Ug,'[V]',@Ug_changed,10,y_offs,60,130,180);
            obj.input_Rg = input_field(obj.panel,'on','Rg',param.Rg,sprintf('[\x3a9]'),@Rg_changed,10,y_offs-1*y_diff,60,130,180);
            obj.input_Ls = input_field(obj.panel,'off','Ls',param.Ls*1e3,'[mH]',@Ls_changed,10,y_offs-2*y_diff,60,130,180);
    end

    % add model settings
    function obj = add_model_settings(fig,param)
        
            % settings
            y_offs = 5;    
            
            obj.panel = uipanel(fig,'Title','Modell Einstellungen','FontSize',font_size,...
                        'Units','pixels');
                    
            obj.model_type = uicontrol(obj.panel,'Style', 'popup',...
                        'String', {'Richard Small Modell', 'Erweitertes Modell'},...
                        'Position', [20 y_offs 180 30],...
                        'Callback', @model_select); 
    end

    % add mechanical model parameters
    function obj = add_mech_model_parameters(fig)
        
            % settings
            y_offs = 60;
            y_diff = 25;          
            
            obj.panel = uipanel(fig,'Title','Mechanische Modell Parameter','FontSize',font_size,...
                        'Units','pixels');
                    
            obj.input_RmMa = input_field(obj.panel,'on','Rm,Ma',param.RmMa,'[kg/s]',@RmMa_changed,10,y_offs,60,130,190);
            obj.input_SmMa = input_field(obj.panel,'on','sm,Ma',param.smMa,sprintf('[kg/s\xb2]'),@smMa_changed,10,y_offs-1*y_diff,60,130,190);
            obj.input_mmMk = input_field(obj.panel,'on','m,Mk',param.mmMk,'[g]',@mmMk_changed,10,y_offs-2*y_diff,60,130,190);
    end

    % add akustical model parameters
    function obj = add_akust_model_parameters(fig)
        
            % settings
            y_offs = 290;        
            
            obj.panel = uipanel(fig,'Title','Akustische Modell Parameter','FontSize',font_size,...
                        'Units','pixels');
                    
            obj.enclosure_type = uicontrol(obj.panel,'Style', 'popup',...
                        'String', {'Unendliche Schallwand',sprintf('Geschlossenes Geh\xe4use'),sprintf('Bassreflex Geh\xe4use')},...
                        'Position', [20 y_offs 180 30],...
                        'Callback', @type_select); 
                    
            obj.closed_box = add_closed_box_parameters(obj.panel);
            obj.bassreflex_box = add_bassreflex_box_parameters(obj.panel);
    end

    % add closed box parameters
    function obj = add_closed_box_parameters(panel)
        
            % settings
            y_panel_offs = 60;
            y_offs = 175;
            y_diff = 25;          
            
            obj.panel = uipanel(panel,'Title',sprintf('Geschlossenes Geh\xe4use'),'FontSize',font_size,...
                        'Units','pixels','Visible','off',...
                        'Position', [5 y_panel_offs 225 225]);
                    
            % box filling, mass change and QmgG       
            obj.input_QmVgG = input_field(obj.panel,'on','QmV,gG',param.gG.QmVgG,'[ ]',@QmVgG_changed,0,y_offs,65,135,200);
            obj.input_mgG = input_field(obj.panel,'on','m',param.gG.mgG,'[ ]',@mgG_changed,10,y_offs-1*y_diff,55,125,190);
            obj.input_Kappa = input_field(obj.panel,'on',sprintf('\x03ba'),param.gG.Kappa,'[1 - 1.4]',@Kappa_changed,10,y_offs-2*y_diff,55,125,190);
            
            % characteristic optimisation
            obj.charact_opt = uicontrol(obj.panel,'Style', 'checkbox','String','Charakteristik Optimierung','Value',1,'Position',[15 y_offs-3.3*y_diff 200 30],'Callback',@charact_opt_gG_changed);
            
            % filter settings
            obj.filter_type = uicontrol(obj.panel,'Style', 'popup',...
                        'String', {'Bessel Charakteristik','Butterworth Charakteristik',sprintf('Kritisch ged\xe4mpfte Charakteristik'),'Chebyshev Charakteristik'},...
                        'Position', [15 y_offs-4.5*y_diff 150 30],...
                        'Callback', @filter_select_gG);
            obj.input_k = input_field(obj.panel,'on','k','0.80','',@k_changed_gG,165,y_offs-4.2*y_diff,15,50,100); 
            obj.input_a1 = input_field(obj.panel,'off','a1','0','',@a1_gG_changed,10,y_offs-5.3*y_diff,55,125,190);
            obj.input_alpha = input_field(obj.panel,'off',sprintf('\x03b1'),'0','',@alpha_gG_changed,5,y_offs-6.5*y_diff,30,80,100);
            obj.input_VgG = input_field(obj.panel,'off','VgG','0','[l]',@VgG_changed,95,y_offs-6.5*y_diff,40,90,120);
            
            obj.input_k.text_1.Visible = 'off';
            obj.input_k.input.Visible = 'off';
    end

    % add bassreflex box parameters
    function obj = add_bassreflex_box_parameters(panel)
        
            % settings
            y_panel_offs = 10;
            y_offs = 220;
            y_diff = 25;          
            
            obj.panel = uipanel(panel,'Title',sprintf('Bassreflex Geh\xe4use'),'FontSize',font_size,...
                        'Units','pixels','Visible','off',...
                        'Position', [5 y_panel_offs 225 275]);
                    
            % QL       
            obj.input_Ql = input_field(obj.panel,'on','QL','7','[ ]',@Ql_changed,10,y_offs,55,125,170);
            
            % characteristic optimisation
            obj.charact_opt = uicontrol(obj.panel,'Style', 'checkbox','String','Charakteristik Optimierung','Value',1,'Position',[15 y_offs-1.3*y_diff 200 30],'Callback',@charact_opt_vG_changed);
            
            
            % filter settings
            obj.filter_type = uicontrol(obj.panel,'Style', 'popup',...
                        'String', {'Bessel Charakteristik','Butterworth Charakteristik','Chebyshev Charakteristik'},...
                        'Position', [15 y_offs-2.5*y_diff 140 30],...
                        'Callback', @filter_select_vG);
            obj.input_k = input_field(obj.panel,'on','k','0.80','',@k_changed_vG,155,y_offs-2.2*y_diff,20,60,100); 
            obj.input_a1 = input_field(obj.panel,'off','a1','0','',@a1_changed,10,y_offs-3.3*y_diff,25,65,180);
            obj.input_a2 = input_field(obj.panel,'off','a2','0','',@a2_changed,80,y_offs-3.3*y_diff,25,65,180);
            obj.input_a3 = input_field(obj.panel,'off','a3','0','',@a3_changed,150,y_offs-3.3*y_diff,25,65,180);
            obj.input_h = input_field(obj.panel,'off','h','0','',@h_changed,10,y_offs-4.5*y_diff,50,90,100);
            obj.input_alpha = input_field(obj.panel,'off',sprintf('\x03b1'),'0','',@alpha_vG_changed,100,y_offs-4.5*y_diff,45,85,100);
            obj.input_QgvG = input_field(obj.panel,'off','Qg,vG','0','',@QgvG_changed,10,y_offs-5.5*y_diff,50,90,100);
            obj.input_VvG = input_field(obj.panel,'off','VvG','0','[l]',@VvG_changed,100,y_offs-5.5*y_diff,45,85,110);
            obj.text_1 = uicontrol(obj.panel,'Style', 'text','String','Bassreflexrohr:','Position',[10 y_offs-6.7*y_diff 100 20]);
            obj.input_r = input_field(obj.panel,'on','r','3','[cm]',@r_changed,10,y_offs-7.5*y_diff,25,65,110);
            obj.input_l = input_field(obj.panel,'off','l','10','[cm]',@l_changed,120,y_offs-7.5*y_diff,15,55,100);
            obj.input_lk_1 = input_field(obj.panel,'on','k,innen','0.85','',@lk_1_changed,10,y_offs-8.5*y_diff,60,100,150);
            obj.input_lk_2 = input_field(obj.panel,'on','k,aussen','0.85','',@lk_2_changed,112,y_offs-8.5*y_diff,65,105,105);
            
            obj.input_k.text_1.Visible = 'off';
            obj.input_k.input.Visible = 'off';
    end

    % add input field with labels
    function obj = input_field(panel,modifiable,text_1,text_2,text_3,callback,offs_x,offs_y,pos_x2,pos_x3,length)
        
            obj.text_1 = uicontrol(panel,'Style', 'text', 'String',text_1, 'Position',[offs_x offs_y pos_x2 20]);
            obj.input = uicontrol(panel,'Style', 'edit', 'Enable',modifiable, 'String', text_2, 'Position',[offs_x+pos_x2 offs_y pos_x3-pos_x2 20],'Callback',callback);
            obj.text_2 = uicontrol(panel,'Style', 'text', 'String',text_3, 'Position',[offs_x+pos_x3 offs_y length-pos_x3 20]);
    end
    function obj = input_field_white(panel,modifiable,text_1,text_2,text_3,callback,offs_x,offs_y,pos_x2,pos_x3,length)
        
            obj.text_1 = uicontrol(panel,'Style', 'text', 'BackgroundColor', 'w', 'String',text_1, 'Position',[offs_x offs_y pos_x2 20]);
            obj.input = uicontrol(panel,'Style', 'edit', 'BackgroundColor', 'w', 'Enable',modifiable, 'String', text_2, 'Position',[offs_x+pos_x2 offs_y pos_x3-pos_x2 20],'Callback',callback);
            obj.text_2 = uicontrol(panel,'Style', 'text', 'BackgroundColor', 'w', 'String',text_3, 'Position',[offs_x+pos_x3 offs_y length-pos_x3 20]);
    end

    % add diagram tabs
    function obj = add_diagram_tabs(fig)
        
            obj.tabgroup = uitabgroup(fig,'Units','pixels');
            
            obj.tab_imp = uitab(obj.tabgroup,'Title',sprintf('Z(j\x03c9)'),'Background','white');
                obj.diagram_imp_amp = add_diagram(obj.tab_imp);
                obj.diagram_imp_pha = add_diagram(obj.tab_imp);
                obj.diagram_settings_imp = add_diagram_settings(obj.tab_imp,'imp',2);
            obj.tab_x = uitab(obj.tabgroup,'Title',sprintf('x(j\x03c9)'),'Background','white');
                obj.diagram_x = add_diagram(obj.tab_x);
                obj.diagram_settings_x = add_diagram_settings(obj.tab_x,'x',1);
            obj.tab_G_Pa = uitab(obj.tabgroup,'Title',sprintf('Pa(j\x03c9)'),'Background','white');
                obj.diagram_G_Pa = add_diagram(obj.tab_G_Pa);
                obj.diagram_settings_G_Pa = add_diagram_settings(obj.tab_G_Pa,'G_Pa',1);
            obj.tab_p = uitab(obj.tabgroup,'Title',sprintf('p(j\x03c9)'),'Background','white');
                obj.diagram_p = add_diagram(obj.tab_p);
                obj.input_r = input_field_white(obj.tab_p,'on','Abstand',param.r,'[m]',@dist_changed,80,60,70,120,160);
                obj.input_theta = input_field_white(obj.tab_p,'off','Winkel',param.theta,'[°]',@theta_changed,255,60,60,110,150);
                obj.diagram_settings_p = add_diagram_settings(obj.tab_p,'p',1);
            obj.tab_n = uitab(obj.tabgroup,'Title',sprintf('\x03b7(j\x03c9)'),'Background','white');
                obj.diagram_n = add_diagram(obj.tab_n);
                obj.diagram_settings_n = add_diagram_settings(obj.tab_n,'n',1);
            obj.tab_G_norm = uitab(obj.tabgroup,'Title',sprintf('G(j\x03c9)'),'Background','white');
                obj.diagram_G_norm_amp = add_diagram(obj.tab_G_norm);
                obj.diagram_G_norm_pha = add_diagram(obj.tab_G_norm);
                obj.diagram_settings_G_norm = add_diagram_settings(obj.tab_G_norm,'G_norm',2);
            obj.tab_g = uitab(obj.tabgroup,'Title','g(t)','Background','white');
                obj.diagram_g = add_diagram(obj.tab_g);
                obj.diagram_settings_g = add_diagram_settings(obj.tab_g,'g',1);
            obj.tab_h = uitab(obj.tabgroup,'Title','h(t)','Background','white');
                obj.diagram_h = add_diagram(obj.tab_h);
                obj.diagram_settings_h = add_diagram_settings(obj.tab_h,'h',1);
            obj.tab_group = uitab(obj.tabgroup,'Title',sprintf('\x03c4(j\x03c9)'),'Background','white');
                obj.diagram_group = add_diagram(obj.tab_group);
                obj.diagram_settings_group = add_diagram_settings(obj.tab_group,'group',1);
            obj.tab_v = uitab(obj.tabgroup,'Title',sprintf('v(j\x03c9)'),'Background','white');
                obj.diagram_v = add_diagram(obj.tab_v);
                obj.diagram_settings_v = add_diagram_settings(obj.tab_v,'v',1);
                obj.tab_v.Parent = [];
    end

    function obj = add_diagram(tab)
            obj = axes('Parent',tab,'Units','pixels');
    end

    % add diagram settings
    function obj = add_diagram_settings(tab,name,diagram_type)
            
            obj.panel = uipanel(tab,'Tag',name,'Title','','FontSize',font_size,...
                        'Units','pixels','BackgroundColor', 'w');
                    
            obj.text_1 = uicontrol(obj.panel,'Style', 'text','BackgroundColor', 'w', 'String','Achsenskalierung','Position',[10 7 120 20]);
            obj.scale_mode = uicontrol(obj.panel,'Style', 'popup', 'BackgroundColor', 'w','String', {'Automatisch','Manuell'},...
                                        'Position', [140 1 100 30],'Callback', @diagram_scaling_select);
                                    
            obj.panel_xyz = uipanel(obj.panel,'Tag',name,'FontSize',font_size,'Units','pixels','BackgroundColor','w','BorderType','none','Visible','off','Position', [250 7 420 30]);
            
            obj.text_2 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','x','Position',[5 2 20 20]);
            obj.input_xmin = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[25 4 50 20],'Callback',@axes_scale_value_changed);
            obj.text_3 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','-','Position',[75 2 10 20]);
            obj.input_xmax = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[85 4 50 20],'Callback',@axes_scale_value_changed);
            obj.text_4 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','y1','Position',[150 2 20 20]);
            obj.input_ymin_1 = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[170 4 50 20],'Callback',@axes_scale_value_changed);
            obj.text_5 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','-','Position',[220 2 10 20]);
            obj.input_ymax_1 = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[230 4 50 20],'Callback',@axes_scale_value_changed);
            if (diagram_type == 2)
                obj.text_6 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','y2','Position',[290 2 20 20]);
                obj.input_ymin_2 = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[310 4 50 20],'Callback',@axes_scale_value_changed);
                obj.text_7 = uicontrol(obj.panel_xyz,'Style', 'text','BackgroundColor', 'w', 'String','-','Position',[360 2 10 20]);
                obj.input_ymax_2 = uicontrol(obj.panel_xyz,'Style', 'edit','String','','Position',[370 4 50 20],'Callback',@axes_scale_value_changed);
            end
            
            obj.hold_on = uicontrol(obj.panel,'Style', 'checkbox','BackgroundColor','w','String','Kurven vergleichen','Value',0,'Callback',@compare_curves_changed);
            obj.export = uicontrol(obj.panel,'Style', 'checkbox','BackgroundColor','w','String','Exportieren','Value',0,'Callback',@export_curves_changed);
    end
            
    % convert value to engineering format
    function y = convert_engineering_format(x)
        
        if (x == 0)
            exp = 0;
        else
            exp = floor(log(x)/log(1000));
        end
        
        if (exp == 0 || exp == -1)
            y = sprintf('%g', x);
        else
            y = sprintf('%ge%01d', [x ./ 1000.^exp, 3.*exp].');
        end
    end

    % export ploted curves
    function export_data(filename, x, y, x_name, y_name)
        % check if folder exists
        if (~isfolder('exported'))
            mkdir('exported');
        end  
        
        % write data to file
        data = [x; y];
        fileID = fopen(fullfile('exported',filename),'w');
        fprintf(fileID, '%s;%s\n', x_name, y_name);
        fprintf(fileID, '%f;%f\n', data);
        fclose(fileID); 
    end
    function export_data_2(filename, x, y1, y2, x_name, y1_name, y2_name)
        % check if folder exists
        if (~isfolder('exported'))
            mkdir('exported');
        end  
        
        % write data to file
        data = [x; y1; y2];
        fileID = fopen(fullfile('exported',filename),'w');
        fprintf(fileID, '%s;%s;%s\n', x_name, y1_name, y2_name);
        fprintf(fileID, '%f;%f;%f\n', data);
        fclose(fileID); 
    end

    function resize_ui(hObject,event)
           
        % Get figure width and height
        figure_width = window.figure.Position(3);
        figure_height = window.figure.Position(4);
       
        % set driver parameter panel position
        width = 235;
        height = 185;
        pos_x = 20;
        pos_y = figure_height-height-15;
        window.driver_param.panel.Position = [pos_x pos_y width height];
        
        % set max values panel position
        width = 235;
        height = 85;
        pos_x = 20;
        pos_y = figure_height-height-205;
        window.max_values.panel.Position = [pos_x pos_y width height];

        % set electrical model parameter panel position
        width = 235;
        height = 115;
        pos_x = 20;
        pos_y = figure_height-height-205-90;
        window.electr_model_param.panel.Position = [pos_x pos_y width height];

        % set model settings panel position
        width = 235;
        height = 65;
        pos_x = 20;
        pos_y = figure_height-height-205-90-120;
        window.model_settings.panel.Position = [pos_x pos_y width height];        
        
        % set mechanical model parameter panel position
        width = 235;
        height = 110;
        pos_x = figure_width-width-15;
        pos_y = figure_height-height-15;
        window.mech_model_param.panel.Position = [pos_x pos_y width height];
        
        % set akustical model parameter panel position
        width = 235;
        height = 350;
        pos_x = figure_width-width-15;
      	pos_y = figure_height-height-115-15;
        window.akust_model_param.panel.Position = [pos_x pos_y width height];
       
        % set tabs position
        width = figure_width-235-235-60;
        height = figure_height-15;
        pos_x = 236+30;
        pos_y = figure_height-height;
        window.tabs.tabgroup.Position = [pos_x pos_y width height];
        
        % set diagram position, one diagram
        width = figure_width-235-235-60-100;
        height = figure_height-200;
        pos_x = 70;
        pos_y = figure_height-height-80-10;
        window.tabs.diagram_G_Pa.Position = [pos_x pos_y width height];
        window.tabs.diagram_x.Position = [pos_x pos_y width height];
        window.tabs.diagram_p.Position = [pos_x pos_y+10 width height-10];
        window.tabs.diagram_n.Position = [pos_x pos_y width height];
        window.tabs.diagram_g.Position = [pos_x pos_y width height];
        window.tabs.diagram_h.Position = [pos_x pos_y width height];
        window.tabs.diagram_group.Position = [pos_x pos_y width height];
        window.tabs.diagram_v.Position = [pos_x pos_y width height];
        
        % set diagram position, two diagrams
        width = figure_width-235-235-60-100;
        height = figure_height-240;
        height_diagram = height/2;
        pos_x = 70;
        pos_y_1 = figure_height-height_diagram-80-10;
        pos_y_2 = figure_height-2*height_diagram-130-10;
        window.tabs.diagram_imp_amp.Position = [pos_x pos_y_1 width height_diagram];
        window.tabs.diagram_imp_pha.Position = [pos_x pos_y_2 width height_diagram];
        window.tabs.diagram_G_norm_amp.Position = [pos_x pos_y_1 width height_diagram];
        window.tabs.diagram_G_norm_pha.Position = [pos_x pos_y_2 width height_diagram];
        
        % set diagram settings position
        width = figure_width-235-235-60-20;
        height = 40;
        pos_x = 10;
        pos_y = 7;
        window.tabs.diagram_settings_imp.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_x.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_G_Pa.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_p.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_n.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_G_norm.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_g.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_h.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_group.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_v.panel.Position = [pos_x pos_y width height];
        window.tabs.diagram_settings_imp.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_x.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_G_Pa.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_p.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_n.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_G_norm.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_g.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_h.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_group.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_v.hold_on.Position = [width-260 5 150 30];
        window.tabs.diagram_settings_imp.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_x.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_G_Pa.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_p.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_n.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_G_norm.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_g.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_h.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_group.export.Position = [width-110 5 150 30];
        window.tabs.diagram_settings_v.export.Position = [width-110 5 150 30];
    end

    function update_ui
        window.electr_model_param.input_Ug.input.String = param.Ug;
        window.mech_model_param.input_RmMa.input.String = param.RmMa;
        window.mech_model_param.input_SmMa.input.String = param.smMa;
        window.mech_model_param.input_mmMk.input.String = (1e3)*param.mmMk;
        
        window.akust_model_param.inf_baffle.input_RauS.input.String = param.uS.RauS;
        window.akust_model_param.inf_baffle.input_mauS.input.String = param.uS.mauS;
        
        window.akust_model_param.closed_box.input_a1.input.String = param.gG.a1;
        window.akust_model_param.closed_box.input_QggG.input.String = sprintf('%.4f',param.gG.QggG);
        window.akust_model_param.closed_box.input_alpha.input.String = sprintf('%.4f',param.gG.alpha);
        window.akust_model_param.closed_box.input_VgG.input.String = sprintf('%.4f',(1e3)*param.gG.VgG);
        
        window.akust_model_param.bassreflex_box.input_a1.input.String = sprintf('%.4f',param.vG.a1);
        window.akust_model_param.bassreflex_box.input_a2.input.String = sprintf('%.4f',param.vG.a2);
        window.akust_model_param.bassreflex_box.input_a3.input.String = sprintf('%.4f',param.vG.a3);
        window.akust_model_param.bassreflex_box.input_QgvG.input.String = sprintf('%.4f',param.vG.QgvG);
        window.akust_model_param.bassreflex_box.input_h.input.String = sprintf('%.4f',param.vG.h);
        window.akust_model_param.bassreflex_box.input_alpha.input.String = sprintf('%.4f',param.vG.alpha);
        window.akust_model_param.bassreflex_box.input_VvG.input.String = sprintf('%.4f',(1e3)*param.vG.VvG);
        window.akust_model_param.bassreflex_box.input_l.input.String = sprintf('%.2f',(1e2)*param.vG.l);
        
        plot_diagrams;
    end

    %% manage model paramters
    
    % add parameters 
    function obj = add_param
        
        obj.model = 'small';
        obj.type = 'uS';
        
        obj.Rs = 8;
        obj.fuS = 50;
        obj.QmMk = 5;
        obj.QeMk = 0.35;
        obj.Am = 0.02;
        obj.VaeMa = 0.02;
        obj.xmax = 0.005;
        obj.pmax = 100;
        obj.Rg = 0;
        obj.Ls = 0.000;
        obj.Ug = 6.3218;
        obj.RmMa = 0;
        obj.smMa = 0;
        obj.mmMk = 0;
        obj.QgMk = 0;
        obj.n0 = 0;
        obj.r = 1;
        obj.theta = 0;
        
        obj.uS.RauS = 0;
        obj.uS.mauS = 0;
        obj.uS.RmuS = 0;
        obj.uS.mmuS = 0;
        obj.uS.mmguS = 0;
        obj.uS.Bl = 0;
        obj.uS.TuS = 0;
        obj.uS.krm = 0;
        
        obj.gG.QmVgG = 100;
        obj.gG.mgG = 1;
        obj.gG.mggG = 0;
        obj.gG.Kappa = 1.4;
        obj.gG.charact_opt = 1;
        obj.gG.k = 0.8;
        obj.gG.a1 = 0;
        obj.gG.QmgG = 0;
        obj.gG.QggG = 0;
        obj.gG.alpha = 0;
        obj.gG.VgG = 0;
        obj.gG.TgG = 0;
        obj.gG.RmggG = 0;
        
        obj.vG.xmax = 0.005;
        obj.vG.Ql = 7;
        obj.vG.charact_opt = 1;
        obj.vG.a1 = 0;
        obj.vG.a2 = 0;
        obj.vG.a3 = 0;
        obj.vG.k = 0.8;
        obj.vG.h = 0;
        obj.vG.QgvG = 0;
        obj.vG.alpha = 0;
        obj.vG.Th = 0;
        obj.vG.VvG = 0;
        obj.vG.magBr = 0;
        obj.vG.r = 0.03;
        obj.vG.l = 0.1;
        obj.vG.lk_1 = 0.85;
        obj.vG.lk_2 = 0.85;
        obj.vG.fmin_rBr = 0;
        obj.vG.fmax_rBr = 0;
        obj.vG.fmax_lBr = 0;
    end 

    % recalculate model parameters from basic parameters
    function recalc_param       
        param.smMa = smMa_calc(param.Am,param.VaeMa);
        param.uS.mauS = mauS_calc(param.Am);
        param.uS.RmuS = RmuS_calc(RauS_calc(param.fuS),param.Am);
        param.uS.mmuS = mmuS_calc(param.uS.mauS,param.Am);
        param.uS.mmguS = mmguS_calc(param.fuS, param.smMa);
        param.mmMk = mmMk_calc(param.uS.mmguS, param.uS.mmuS);
        param.RmMa = RmMa_calc(param.QmMk, param.smMa, param.uS.mmguS);
        param.uS.Bl = Bl_calc(param.Rs, param.QeMk, param.smMa, param.uS.mmguS);
        param.uS.TuS = TuS_calc(param.fuS);
        param.uS.krm = krm_calc(param.Am);
        param.QgMk = QgMk_calc(param.QmMk, param.QeMk);
        if (strcmp(param.type, 'uS'))
            param.Ug = Ug_calc_uS(param.pmax, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS);
        else
            param.Ug = Ug_calc(param.pmax, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS);
        end
        if (strcmp(param.type, 'gG'))
            param.n0 = n_function(param.uS.Bl, param.Am, param.Rs, param.gG.mggG, 1);
        else
            param.n0 = n_function(param.uS.Bl, param.Am, param.Rs, param.uS.mmguS, 1);
        end
        
        % closed box
        if (param.gG.charact_opt == 1)
            param.gG.QggG = QggG_calc(param.gG.a1);
        end
        param.gG.mggG = mggG_calc(param.gG.mgG, param.uS.mmguS);
        param.gG.alpha = alpha_gG_calc(param.gG.QggG, param.QmMk, param.QeMk, param.gG.QmVgG, param.gG.mgG);
        param.gG.VgG = VgG_calc(param.gG.alpha,param.gG.Kappa,param.VaeMa);
        param.gG.TgG = TgG_calc(param.fuS, param.gG.alpha, param.gG.mgG);
        param.gG.RmggG = RmggG_calc(param.RmMa, param.smMa, param.gG.alpha, param.gG.mggG, param.gG.QmVgG);
        param.gG.QmgG = QmgG_calc(param.gG.RmggG, param.smMa, param.gG.alpha, param.gG.mggG);
        
        % bassreflex box
        if (param.vG.charact_opt == 1)
            [param.vG.h,param.vG.Th,param.vG.QgvG,param.vG.alpha] = calculate_basis_param_vG(param.vG.a1,param.vG.a2,param.vG.a3,param.vG.Ql,param.uS.TuS);
        end
        param.vG.VvG = VvG_calc(param.vG.alpha,param.VaeMa);
        param.vG.magBr = magBr_calc(param.vG.VvG,param.vG.Th);
        if (param.vG.charact_opt == 1)
            param.vG.l = l_Br_calc(param.vG.r,param.vG.lk_1,param.vG.lk_2,param.vG.magBr);
        end
        param.vG.fmin_rBr = fmin_rBr_calc(param.vG.r);
        param.vG.fmax_rBr = fmax_rBr_calc(param.vG.r);
        param.vG.fmax_lBr = fmax_lBr_calc(param.vG.l);
        
    end

    %% update diagram plots
    
    function plot_diagrams
        
        f = 1:1:20e3;
        w = 2*pi*f;   
        
        if (strcmp(param.model, 'small'))  
            if (strcmp(param.type, 'uS'))            
                Z = imp_function(w, param.uS.TuS, param.QmMk, param.RmMa, param.uS.Bl, param.Rs);
                x = x_function(w, param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.uS.TuS, param.QgMk);
                G_norm = G_norm_function(w, param.uS.TuS, param.QgMk);
                G_norm_s = G_norm_function_s(param.uS.TuS, param.QgMk);
                G_norm_optimal_s = 1;
                Pa = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm);
                Pa_optimal = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, 1);
                pa = p_function_uS(param.Ug, param.r, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm);
                pa_optimal = p_function_uS(param.Ug, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, 1);
                n = n_function(param.uS.Bl, param.Am, param.Rs, param.uS.mmguS, G_norm);
                n_optimal = n_function(param.uS.Bl, param.Am, param.Rs, param.uS.mmguS, 1);
            end

            if (strcmp(param.type, 'gG'))   
                Z = imp_function(w, param.gG.TgG, param.gG.QmgG, param.gG.RmggG, param.uS.Bl, param.Rs);
                x = x_function(w, param.Ug, param.Rg, param.Rs, param.uS.Bl, param.gG.mggG, param.gG.TgG, param.gG.QggG);
                G_norm = G_norm_function(w, param.gG.TgG, param.gG.QggG);
                G_norm_s = G_norm_function_s(param.gG.TgG, param.gG.QggG);
                G_norm_optimal = G_norm_characteristic_gG(w, param.gG.TgG, param.gG.a1);
                G_norm_optimal_s = G_norm_characteristic_gG_s(param.gG.TgG, param.gG.a1);
                Pa = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.gG.mggG, G_norm);
                Pa_optimal = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.gG.mggG, G_norm_optimal);
                pa = p_function(param.Ug, param.r, param.uS.Bl, param.Am, param.Rg, param.Rs, param.gG.mggG, G_norm);
                pa_optimal = p_function(param.Ug, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.gG.mggG, G_norm_optimal);
                n = n_function(param.uS.Bl, param.Am, param.Rs, param.gG.mggG, G_norm);
                n_optimal = n_function(param.uS.Bl, param.Am, param.Rs, param.gG.mggG, G_norm_optimal);
            end

            if (strcmp(param.type, 'vG'))
                Z = imp_function_vG(w, param.uS.TuS, param.vG.Th, param.QmMk, param.vG.Ql, param.vG.alpha, param.RmMa, param.uS.Bl, param.Rs);
                x = x_function_vG(w, param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.vG.Th, param.uS.TuS, param.vG.QgvG, param.vG.Ql, param.vG.alpha);
                G_norm = G_norm_function_vG(w, param.vG.Th, param.uS.TuS, param.vG.QgvG, param.vG.Ql, param.vG.alpha);
                G_norm_s = G_norm_function_vG_s(param.vG.Th, param.uS.TuS, param.vG.QgvG, param.vG.Ql, param.vG.alpha);
                G_norm_optimal = G_norm_characteristic_vG(w, param.vG.Th, param.uS.TuS, param.vG.a1, param.vG.a2, param.vG.a3);
                G_norm_optimal_s = G_norm_characteristic_vG_s(param.vG.Th, param.uS.TuS, param.vG.a1, param.vG.a2, param.vG.a3);
                Pa = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm);
                Pa_optimal = Pa_function(param.Ug, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm_optimal);
                pa = p_function(param.Ug, param.r, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm);
                pa_optimal = p_function(param.Ug, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS, G_norm_optimal);
                n = n_function(param.uS.Bl, param.Am, param.Rs, param.uS.mmguS, G_norm);
                n_optimal = n_function(param.uS.Bl, param.Am, param.Rs, param.uS.mmguS, G_norm_optimal);
                v = v_function_vG(w, param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.vG.Th, param.uS.TuS, param.vG.QgvG, param.vG.Ql, param.vG.alpha, param.Am, param.vG.r);
            end
            
            plot_impedance(w,Z);
            plot_x(w,x);
            plot_Pa(w,Pa,Pa_optimal);
            plot_p(w,pa,pa_optimal);
            plot_n(w,n,n_optimal);
            plot_G_norm(w,G_norm_s,G_norm_optimal_s);
            plot_g(G_norm_s,G_norm_optimal_s);
            plot_h(G_norm_s,G_norm_optimal_s);
            plot_group(w,G_norm_s,G_norm_optimal_s);
            if (strcmp(param.type, 'vG'))
                plot_v(w,v);
            end
        
        else    % advanced model
            if (strcmp(param.type, 'uS'))               
                [Zges,Za,p,q] = matrix_model_calc(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.Am, w);
                Pa = Pa_matrix_model_calc(q, Za);
                pa = p_matrix_model_calc_uS(q, param.r, param.theta, param.Am, w);
                n = n_matrix_model_calc(param.Ug, Zges, Pa);
                x = x_matrix_model_calc(q, param.Am, w);
                G_norm_s = G_norm_matrix_model(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.uS.mmguS, param.Am);
                G_norm_optimal = G_norm_matrix_model_optimal_uS(param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.Am, pa, w);
            end
            
            if (strcmp(param.type, 'gG'))               
                [Zges,Za,p,q] = matrix_model_calc_gG(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.Am, param.gG.alpha, param.gG.TgG, param.gG.mggG, param.gG.QmVgG, w);
                Pa = Pa_matrix_model_calc(q, Za);
                pa = p_matrix_model_calc(q, param.r, param.theta, param.Am, w);
                n = n_matrix_model_calc(param.Ug, Zges, Pa);
                x = x_matrix_model_calc(q, param.Am, w);
                G_norm_s = G_norm_matrix_model_gG(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.uS.mmguS, param.Am, param.gG.alpha, param.gG.TgG, param.gG.mggG, param.gG.QmVgG);
                G_norm_optimal = G_norm_matrix_model_optimal(param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.Am, pa, w);
            end
            
            if (strcmp(param.type, 'vG'))               
                [Zges,Zm,qm,qbr,qa] = matrix_model_calc_vG(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.Am, param.vG.alpha, param.vG.Ql, param.vG.Th, param.vG.r, param.vG.l, w);
                Pa = Pa_matrix_model_calc(qa, Zm);
                pa = p_matrix_model_calc(qa, param.r, param.theta, param.Am, w);
                n = n_matrix_model_calc(param.Ug, Zges, Pa);
                x = x_matrix_model_calc(qm, param.Am, w);
                v = v_matrix_model_calc(qbr, param.vG.r);
                G_norm_s = G_norm_matrix_model_vG(param.Ug, param.Rg, param.Rs, param.Ls, param.uS.Bl, param.RmMa, param.smMa, param.mmMk, param.Am, param.vG.alpha, param.uS.mmguS, param.vG.Ql, param.vG.Th, param.vG.r, param.vG.l);
                G_norm_optimal = G_norm_matrix_model_optimal(param.Ug, param.Rg, param.Rs, param.uS.Bl, param.uS.mmguS, param.Am, pa, w);
            end
            
            plot_impedance(w,Zges);
            plot_x(w,x);
            plot_Pa(w,Pa,1);
            plot_p(w,pa,1);
            plot_n(w,n,1);
            plot_G_norm(w,G_norm_s,G_norm_optimal);
            plot_g(G_norm_s,1);
            plot_h(G_norm_s,1);
            plot_group(w,G_norm_s,1); 
            if (strcmp(param.type, 'vG'))
                plot_v(w,v);
            end
        end
    end

    % plot curves
    function plot_impedance(w,Z)
        
        diagram = window.tabs.diagram_imp_amp;
        
        if (window.tabs.diagram_settings_imp.hold_on.Value == 0)
            hold(diagram,'off');
        end

        f = w/(2*pi);
        semilogx(diagram,f, abs(Z),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_imp.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
        else
            x_min = str2double(window.tabs.diagram_settings_imp.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_imp.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_imp.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_imp.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end         
        if (strcmp(param.type, 'vG'))
            [value,index] = min(abs(f-1/(param.vG.Th*2*pi)));
            plot(diagram,f(index),abs(Z(index)),'kx');
            text(1/(param.vG.Th*2*pi)*(1-0.1), abs(Z(index))*(1+0.8), '$f_{H}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        else
            if (strcmp(param.type, 'uS'))
                [value,index] = min(abs(f-param.fuS));
                plot(diagram,f(index),abs(Z(index)),'kx');
                text(param.fuS*(1-0.1), abs(Z(index))*(1+0.1), '$f_{uS}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
            end
            if (strcmp(param.type, 'gG'))
                [value,index] = min(abs(f-1/param.gG.TgG/(2*pi)));
                plot(diagram,f(index),abs(Z(index)),'kx');
                text(1/(param.gG.TgG*(2*pi))*(1-0.1), abs(Z(index))*(1+0.1), '$f_{gG}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
            end
        end
        if (window.tabs.diagram_settings_imp.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        ylabel(diagram,'$|Z| \, [\Omega]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Impedanz}','FontSize',12,'interpreter','latex');
        
        diagram = window.tabs.diagram_imp_pha;
        
        if (window.tabs.diagram_settings_imp.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        semilogx(diagram,f, 180/pi*angle(Z),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_imp.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
        else
            y_min = str2double(window.tabs.diagram_settings_imp.input_ymin_2.String);
            y_max = str2double(window.tabs.diagram_settings_imp.input_ymax_2.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end 
        if (strcmp(param.type, 'vG'))
            [value,index] = min(abs(f-1/(param.vG.Th*2*pi)));
            plot(diagram,f(index),180/pi*angle(Z(index)),'kx');
            text(1/(param.vG.Th*2*pi)*(1-0.2), 180/pi*angle(Z(index))+20, '$f_{H}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        else
            if (strcmp(param.type, 'uS'))
                [value,index] = min(abs(f-param.fuS));
                plot(diagram,f(index),180/pi*angle(Z(index)),'kx');
                text(param.fuS*(1+0.05), 180/pi*angle(Z(index))+20, '$f_{uS}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
            end
            if (strcmp(param.type, 'gG'))
                [value,index] = min(abs(f-1/param.gG.TgG/(2*pi)));
                plot(diagram,f(index),180/pi*angle(Z(index)),'kx');
                text(1/(param.gG.TgG*2*pi)*(1+0.05), 180/pi*angle(Z(index))+20, '$f_{gG}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
            end
        end
        if (window.tabs.diagram_settings_imp.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$arg(Z) \, [^\circ]$','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_imp.export.Value == 1)
            export_data_2('impedance_data_exported.txt', f, abs(Z), 180/pi*angle(Z), 'f [Hz]', '|Z| [Ohm]', 'arg(Z) [°]');
        end
    end

    function plot_x(w,x)
        
        diagram = window.tabs.diagram_x;
        
        if (window.tabs.diagram_settings_x.hold_on.Value == 0)
            hold(diagram,'off');
        end

        f = w/(2*pi);
        semilogx(diagram,f, (1e3)*abs(x),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_x.scale_mode.Value == 1)
            xlim = [20 20e3];
            diagram.XLim = xlim;
            ylim = [0 ceil((1e3)*param.xmax+0.5)];
            diagram.YLim = ylim;
        else
            x_min = str2double(window.tabs.diagram_settings_x.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_x.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_x.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_x.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
            xlim = [x_min x_max];
            ylim = [y_min y_max];
        end
        semilogx(diagram, xlim, [(1e3)*param.xmax (1e3)*param.xmax], 'Color', 'red');
        text(1e3, (1e3)*param.xmax*(1+0.05), '$x_{max}$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        if (strcmp(param.type, 'vG'))
            y = (1e3)*abs(x);
            [value,index] = min(abs(f-1/(param.vG.Th*2*pi)));
            plot(diagram,f(index),y(index),'kx');
            text(1/(param.vG.Th*2*pi)*(1-0.1), y(index)*(1+1.5), '$f_{H}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        end
        if (window.tabs.diagram_settings_x.hold_on.Value == 0)
            hold(diagram,'off');
        end       
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$x_M \, [mm]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Membranauslenkung}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_x.export.Value == 1)
            export_data('x_data_exported.txt', f, (1e3)*abs(x), 'f [Hz]', 'x [mm]');
        end
    end

    function plot_Pa(w,Pa,Pa_optimal)
        
        diagram = window.tabs.diagram_G_Pa;
        
        if (window.tabs.diagram_settings_G_Pa.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            magn_db = 10*log10(abs(Pa_optimal)/(1e-12));
            semilogx(diagram,f, magn_db,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        magn_db = 10*log10(abs(Pa)/(1e-12));
        semilogx(diagram,f, magn_db,'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_G_Pa.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
            ylim = get(diagram,'YLim');
        else
            x_min = str2double(window.tabs.diagram_settings_G_Pa.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_G_Pa.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_G_Pa.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_G_Pa.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
            ylim = [y_min y_max];
        end
        semilogx(diagram,[param.uS.krm param.uS.krm], ylim, '--', 'Color', 'red');
        text(param.uS.krm*(1+0.1), (ylim(1)+ylim(2))/1.9, '$kr_M=1$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        [y_max,index] = max(magn_db);
        [value,index] = min(abs(magn_db(1:index)-(y_max-3)));
        plot(diagram,f(index),magn_db(index),'kx');
        text(f(index)*(1-0.3), magn_db(index)*(1+0.02), '$f_{3dB}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        if (window.tabs.diagram_settings_G_Pa.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$Pa \, [dB]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Akustische Leistung}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_G_Pa.export.Value == 1)
            export_data('Pa_data_exported.txt', f, magn_db, 'f [Hz]', 'Pa [dB]');
        end
    end

    function plot_p(w,pa,pa_optimal)
        
        diagram = window.tabs.diagram_p;
        
        if (window.tabs.diagram_settings_p.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            magn_db = 20*log10(abs(pa_optimal)/(20e-6));
            semilogx(diagram,f, magn_db,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        magn_db = 20*log10(abs(pa)/(20e-6));
        semilogx(diagram,f, magn_db(:),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_p.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
            ylim = get(diagram,'YLim');
        else
            x_min = str2double(window.tabs.diagram_settings_p.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_p.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_p.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_p.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
            ylim = [y_min y_max];
        end
        semilogx(diagram,[param.uS.krm param.uS.krm], ylim, '--', 'Color', 'red');
        text(param.uS.krm*(1+0.1), (ylim(1)+ylim(2))/1.9, '$kr_M=1$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        [y_max,index] = max(magn_db);
        [value,index] = min(abs(magn_db(1:index)-(y_max-3)));
        plot(diagram,f(index),magn_db(index),'kx');
        text(f(index)*(1-0.3), magn_db(index)*(1+0.02), '$f_{3dB}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        if (window.tabs.diagram_settings_p.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$p_a \, [dB]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Schalldruckpegel}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_p.export.Value == 1)
            export_data('pa_data_exported.txt', f, magn_db, 'f [Hz]', 'pa [dB]');
        end
    end

    function plot_n(w,n,n_optimal)
        
        diagram = window.tabs.diagram_n;
        
        if (window.tabs.diagram_settings_n.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            semilogx(diagram,f, 100*abs(n_optimal),'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        semilogx(diagram,f, 100*abs(n),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_n.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
        else
            x_min = str2double(window.tabs.diagram_settings_n.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_n.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_n.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_n.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end
        if (strcmp(param.model, 'advanced'))
            y = abs(n);
            [y_max,index] = max(y);
            [value,index_2] = min(abs(y(index:end)-param.n0));
            plot(diagram,f(index+index_2-1),100*y(index+index_2-1),'kx');
            text(f(index+index_2-1)*(1-0.05), 100*y(index+index_2-1)*(1+0.15), '$\eta_0$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        end
        if (window.tabs.diagram_settings_n.hold_on.Value == 0)
            hold(diagram,'off');
        end       
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$\eta \, [\%]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Wirkungsgrad}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_n.export.Value == 1)
            export_data('eta_data_exported.txt', f, 100*abs(n), 'f [Hz]', 'eta [%]');
        end
    end

    function plot_G_norm(w,G_norm,G_norm_optimal)
        
        diagram = window.tabs.diagram_G_norm_amp;
        
        if (window.tabs.diagram_settings_G_norm.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            [magn,phase] = bode(G_norm_optimal,w);
            magn_db = 20*log10(magn(:));
            semilogx(diagram,f, magn_db,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        if (strcmp(param.model, 'advanced'))
            magn_db = 20*log10(abs(G_norm_optimal));
            semilogx(diagram,f, magn_db,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        [magn,phase] = bode(G_norm,w);
        magn_db = 20*log10(magn(:));
        semilogx(diagram,f, magn_db,'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_G_norm.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
            diagram.YLim(2) = 10;
        else
            x_min = str2double(window.tabs.diagram_settings_G_norm.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_G_norm.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_G_norm.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_G_norm.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end
        [y_max,index] = max(magn_db);
        [value,index] = min(abs(magn_db(1:index)-(y_max-3)));
        plot(diagram,f(index),magn_db(index),'kx');
        text(f(index)*(1-0.4), magn_db(index)*(1-0.8), '$f_{3dB}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        if (window.tabs.diagram_settings_G_norm.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        ylabel(diagram,'$|G| \, [dB]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Normierter Schalldruckpegel}','FontSize',12,'interpreter','latex');
        
        diagram = window.tabs.diagram_G_norm_pha;
        
        if (window.tabs.diagram_settings_G_norm.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            [magn,phase] = bode(G_norm_optimal,w);
            semilogx(diagram,f, phase(:),'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        if (strcmp(param.model, 'advanced'))
            if (angle(G_norm_optimal(1)) < 0)
                phase_offset = 360;
            else
                phase_offset = 0;
            end
            semilogx(diagram,f, phase_offset+180/pi*unwrap(angle(G_norm_optimal)),'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        [magn,phase] = bode(G_norm,w);
        semilogx(diagram,f, phase(:),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_G_norm.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
        else
            y_min = str2double(window.tabs.diagram_settings_G_norm.input_ymin_2.String);
            y_max = str2double(window.tabs.diagram_settings_G_norm.input_ymax_2.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end
        [y_max,index] = max(magn_db);
        [value,index] = min(abs(magn_db(1:index)-(y_max-3)));
        plot(diagram,f(index),phase(index),'kx');
        text(f(index)*(1+0.1), phase(index)*(1+0.2), '$f_{3dB}$', 'FontSize', 12, 'Color', 'k','Parent', diagram,'interpreter','latex');
        if (window.tabs.diagram_settings_G_norm.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$arg(G) \, [^\circ]$','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_G_norm.export.Value == 1)
            export_data_2('Gnorm_data_exported.txt', f, magn_db', phase(:)', 'f [Hz]', '|G| [dB]', 'arg(G) [°]');
        end
    end

    function plot_g(G_norm,G_norm_optimal)
        
        diagram = window.tabs.diagram_g;
        
        if (window.tabs.diagram_settings_g.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        if(window.tabs.diagram_settings_g.scale_mode.Value == 1)
            t_diff = round(1/param.fuS *100)/100;
            t = -t_diff:t_diff/1000:4*t_diff;
        else
            x_min = str2double(window.tabs.diagram_settings_g.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_g.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_g.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_g.input_ymax_1.String);
            t = (1e-3)*x_min:(1e-3)*x_max/1000:(1e-3)*x_max;
        end
        
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            [Y,x] = impulse(G_norm_optimal,t);
            Y_d = [zeros(length(t)-length(x),1); Y];
            plot(diagram,(1e3)*t,Y_d,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        [Y,x] = impulse(G_norm,t);
        Y_d = [zeros(length(t)-length(x),1); Y];
        plot(diagram,(1e3)*t,Y_d,'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_g.scale_mode.Value == 1)
            diagram.XLim = [-t_diff*1e3 4*t_diff*1e3];
            ylim = get(diagram,'YLim');
        else
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
            ylim = [y_min y_max];
        end
        plot(diagram,[0 0],[0 ylim(2)],'k');
        
        if (window.tabs.diagram_settings_g.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$t \, [ms]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$U \, [V]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Impulsantwort}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_g.export.Value == 1)
            export_data('g_data_exported.txt', (1e3)*t, Y_d', 't [ms]', 'U [V]');
        end
    end
    
    function plot_h(G_norm,G_norm_optimal)
 
        diagram = window.tabs.diagram_h;
        
        if (window.tabs.diagram_settings_h.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        if(window.tabs.diagram_settings_h.scale_mode.Value == 1)
            t_diff = round(1/param.fuS *100)/100;
            t = -t_diff:t_diff/1000:4*t_diff;
        else
            x_min = str2double(window.tabs.diagram_settings_h.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_h.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_h.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_h.input_ymax_1.String);
            t = (1e-3)*x_min:(1e-3)*x_max/1000:(1e-3)*x_max;
        end

        Y_step = (t>0);
        plot(diagram,(1e3)*t,Y_step,'k');
        hold(diagram,'on');
        
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            Y = step(G_norm_optimal,t);
            Y_d = [zeros(length(t)-length(Y),1); Y];
            plot(diagram,(1e3)*t,Y_d,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        Y = step(G_norm,t);
        Y_d = [zeros(length(t)-length(Y),1); Y];
        plot(diagram,(1e3)*t,Y_d,'b');
        if(window.tabs.diagram_settings_h.scale_mode.Value == 1)
            diagram.XLim = [-t_diff*1e3 4*t_diff*1e3];
            diagram.YLim(2) = 1.2;
        else
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end
        if (window.tabs.diagram_settings_h.hold_on.Value == 0)
            hold(diagram,'off');
        end
      
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$t \, [ms]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$U \, [V]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Sprungantwort}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_h.export.Value == 1)
            export_data('h_data_exported.txt', (1e3)*t, Y_d', 't [ms]', 'U [V]');
        end
    end

    function plot_group(w,G_norm,G_norm_optimal)
        
        diagram = window.tabs.diagram_group;
        
        if (window.tabs.diagram_settings_group.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        if (strcmp(param.type, 'uS') == 0 && strcmp(param.model, 'small'))
            [magn,phase] = bode(G_norm_optimal,w);
            group = -gradient(phase(:),w); 
            semilogx(diagram,f, group,'Color', [0,0,0]+0.5);
            hold(diagram,'on');
        end
        [magn,phase] = bode(G_norm,w);
        group = -gradient(phase(:),w); 
        semilogx(diagram,f, group,'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_group.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
        else
            x_min = str2double(window.tabs.diagram_settings_group.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_group.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_group.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_group.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
        end
        if (window.tabs.diagram_settings_group.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$t \, [s]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Gruppenlaufzeit}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_group.export.Value == 1)
            export_data('group_data_exported.txt', f, group', 'f [Hz]', 't [s]');
        end
    end

    function plot_v(w,v)
        
        diagram = window.tabs.diagram_v;
        
        if (window.tabs.diagram_settings_v.hold_on.Value == 0)
            hold(diagram,'off');
        end
        
        f = w/(2*pi);
        semilogx(diagram,f, abs(v),'b');
        hold(diagram,'on');
        if(window.tabs.diagram_settings_v.scale_mode.Value == 1)
            diagram.XLim = [20 20e3];
            ylim = get(diagram,'YLim');
        else
            x_min = str2double(window.tabs.diagram_settings_v.input_xmin.String);
            x_max = str2double(window.tabs.diagram_settings_v.input_xmax.String);
            y_min = str2double(window.tabs.diagram_settings_v.input_ymin_1.String);
            y_max = str2double(window.tabs.diagram_settings_v.input_ymax_1.String);
            diagram.XLim = [x_min x_max];
            diagram.YLim = [y_min y_max];
            ylim = [y_min y_max];
        end
        semilogx(diagram,[param.vG.fmin_rBr param.vG.fmin_rBr], ylim, 'Color', 'red');
        text(param.vG.fmin_rBr*(1+0.1), (ylim(1)+ylim(2))/1.9, '$f_{min,r_{Br}}$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        semilogx(diagram,[param.vG.fmax_rBr param.vG.fmax_rBr], ylim, 'Color', 'red');
        text(param.vG.fmax_rBr*(1+0.1), (ylim(1)+ylim(2))/1.9*0.95, '$f_{max,r_{Br}}$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        semilogx(diagram,[param.vG.fmax_lBr param.vG.fmax_lBr], ylim, 'Color', 'red');
        text(param.vG.fmax_lBr*(1+0.1), (ylim(1)+ylim(2))/1.9*0.9, '$f_{max,l_{Br}}$', 'FontSize', 12, 'Color', 'red','Parent', diagram,'interpreter','latex');
        if (window.tabs.diagram_settings_v.hold_on.Value == 0)
            hold(diagram,'off');
        end
        diagram.XGrid = 'on';
        diagram.YGrid = 'on';
        xlabel(diagram,'$f \, [Hz]$','FontSize',12,'interpreter','latex');
        ylabel(diagram,'$v_{Br} \, [m/s]$','FontSize',12,'interpreter','latex');
        title(diagram,'\textbf{Luftgeschwindigkeit im Bassreflexrohr}','FontSize',12,'interpreter','latex');
        
        if (window.tabs.diagram_settings_v.export.Value == 1)
            export_data('v_data_exported.txt', f, abs(v), 'f [Hz]', 'v_br [m/s]');
        end
    end

    %% manage user inputs
  
    function Rs_changed(hObject, ~)
        param.Rs = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function fuS_changed(hObject, ~)
        param.fuS = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function QmMk_changed(hObject, ~)
        param.QmMk = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function QeMk_changed(hObject, ~)
        param.QeMk = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end
 
    function Am_changed(hObject, ~)
        param.Am = (1e-4)*str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function VaeMa_changed(hObject, ~)
        param.VaeMa = (1e-3)*str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function xmax_changed(hObject, ~)
        param.xmax = (1e-3)*str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function pmax_changed(hObject, ~)
        param.pmax = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function Ug_changed(hObject, ~)
        param.Ug = str2double(get(hObject,'String'));
        if (strcmp(param.type, 'uS'))
            param.pmax = Ug_calc_uS_inv(param.Ug, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS);
        else
            param.pmax = Ug_calc_inv(param.Ug, 1, param.uS.Bl, param.Am, param.Rg, param.Rs, param.uS.mmguS);
        end
        window.max_values.input_pmax.input.String = param.pmax;
        recalc_param;
        update_ui;
    end

    function Rg_changed(hObject, ~)
        param.Rg = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function Ls_changed(hObject, ~)
        param.Ls = (1e-3)*str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function model_select(hObject, ~)
        value = get(hObject,'Value');
        
        if (value == 1)
            param.model = 'small';
            window.electr_model_param.input_Ls.input.Enable = 'off';
            window.tabs.input_theta.input.Enable = 'off';
            window.akust_model_param.closed_box.input_mgG.input.Enable = 'on';
        else
            param.model = 'advanced';
            window.electr_model_param.input_Ls.input.Enable = 'on';
            window.tabs.input_theta.input.Enable = 'on';
            window.akust_model_param.closed_box.input_mgG.input.Enable = 'off';
        end
        
        recalc_param;
        update_ui;
    end

    function RmMa_changed(hObject, ~)
        param.RmMa = str2double(get(hObject,'String'));
        param.QmMk = RmMa_calc_inv(param.RmMa, param.smMa, param.uS.mmguS);
        window.driver_param.input_QmMk.input.String = param.QmMk;
        recalc_param;
        update_ui;
    end

    function smMa_changed(hObject, ~)
        param.smMa = str2double(get(hObject,'String'));
        param.VaeMa = smMa_calc_inv(param.Am,param.smMa);
        window.driver_param.input_VaeMa.input.String = (1e3)*param.VaeMa;
        recalc_param;
        update_ui;
    end

    function mmMk_changed(hObject, ~)
        param.mmMk = (1e-3)*str2double(get(hObject,'String'));
        param.uS.mmguS = mmMk_calc_inv(param.mmMk, param.uS.mmuS);
        param.fuS = mmguS_calc_inv(param.uS.mmguS, param.smMa);
        window.driver_param.input_fuS.input.String = param.fuS;
        recalc_param;
        update_ui;
    end

    function dist_changed(hObject, ~)
            param.r = str2double(get(hObject,'String'));
            recalc_param;
            update_ui;
    end
    
    function theta_changed(hObject, ~)
        param.theta = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function type_select(hObject, ~)
        value = get(hObject,'Value');
        
        if (value == 1)
            window.akust_model_param.closed_box.panel.Visible = 'off';
            window.akust_model_param.bassreflex_box.panel.Visible = 'off';
            window.tabs.tab_v.Parent = [];
            param.type = 'uS';
        end
        
        if (value == 2)
            window.akust_model_param.bassreflex_box.panel.Visible = 'off';
            window.akust_model_param.closed_box.panel.Visible = 'on';
            window.tabs.tab_v.Parent = [];
            param.type = 'gG';
        end
        
        if (value == 3)           
            window.akust_model_param.closed_box.panel.Visible = 'off';
            window.akust_model_param.bassreflex_box.panel.Visible = 'on';
            window.tabs.tab_v.Parent = window.tabs.tabgroup;
            param.type = 'vG';
        end
        
        recalc_param;
        update_ui;
    end

    % close box
    function QmVgG_changed(hObject, ~)
        param.gG.QmVgG = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function mgG_changed(hObject, ~)
        param.gG.mgG = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function Kappa_changed(hObject, ~)
        param.gG.Kappa = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function charact_opt_gG_changed(hObject, ~)       
        param.gG.charact_opt = get(hObject,'Value');
        recalc_param;
        update_ui;
        
        if(param.gG.charact_opt)
            window.akust_model_param.closed_box.input_alpha.input.Enable = 'off';
            window.akust_model_param.closed_box.input_QggG.input.Enable = 'off';
            window.akust_model_param.closed_box.input_VgG.input.Enable = 'off';
        else
            window.akust_model_param.closed_box.input_alpha.input.Enable = 'on';
            window.akust_model_param.closed_box.input_QggG.input.Enable = 'on';
            window.akust_model_param.closed_box.input_VgG.input.Enable = 'on';            
        end
    end

    function filter_select_gG(hObject, ~)
        value = get(hObject,'Value');
        
        if (value == 1)
            window.akust_model_param.closed_box.input_k.text_1.Visible = 'off';
            window.akust_model_param.closed_box.input_k.input.Visible = 'off';
            param.gG.a1 = Bessel_coeff_calc_gG();
        end
        
        if (value == 2)
            window.akust_model_param.closed_box.input_k.text_1.Visible = 'off';
            window.akust_model_param.closed_box.input_k.input.Visible = 'off';
            param.gG.a1 = Butterworth_coeff_calc_gG();
        end
        
        if (value == 3)
            window.akust_model_param.closed_box.input_k.text_1.Visible = 'off';
            window.akust_model_param.closed_box.input_k.input.Visible = 'off';
            param.gG.a1 = Critical_damped_coeff_calc_gG();
        end
        
        if (value == 4)            
            window.akust_model_param.closed_box.input_k.text_1.Visible = 'on';
            window.akust_model_param.closed_box.input_k.input.Visible = 'on';
            param.gG.a1 = Chebyshev_coeff_calc_gG(param.gG.k);
        end

        recalc_param;
        update_ui;
        
    end

    function k_changed_gG(hObject, ~)
        param.gG.k = str2double(get(hObject,'String'));
        param.gG.a1 = Chebyshev_coeff_calc_gG(param.gG.k);
        recalc_param;
        update_ui;
    end

    function alpha_gG_changed(hObject, ~)
        param.gG.alpha = str2double(get(hObject,'String'));
        param.gG.QggG = alpha_gG_calc_inv(param.QmMk, param.QeMk, param.gG.QmVgG, param.gG.alpha, param.gG.mgG);
        recalc_param;
        update_ui;
    end

    function VgG_changed(hObject, ~)
        param.gG.VgG = (1e-3)*str2double(get(hObject,'String'));
        param.gG.alpha = VgG_calc_inv(param.gG.VgG,param.gG.Kappa,param.VaeMa);
        param.gG.QggG = alpha_gG_calc_inv(param.QmMk, param.QeMk, param.gG.QmVgG, param.gG.alpha, param.gG.mgG);
        recalc_param;
        update_ui;
    end

    % bassreflex box
    function Ql_changed(hObject, ~)
        param.vG.Ql = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function charact_opt_vG_changed(hObject, ~)
        param.vG.charact_opt = get(hObject,'Value');
        recalc_param;
        update_ui;    
        
        if(param.vG.charact_opt)
            window.akust_model_param.bassreflex_box.input_h.input.Enable = 'off';
            window.akust_model_param.bassreflex_box.input_alpha.input.Enable = 'off';
            window.akust_model_param.bassreflex_box.input_QgvG.input.Enable = 'off';
            window.akust_model_param.bassreflex_box.input_VvG.input.Enable = 'off';
            window.akust_model_param.bassreflex_box.input_l.input.Enable = 'off';
        else
            window.akust_model_param.bassreflex_box.input_h.input.Enable = 'on';
            window.akust_model_param.bassreflex_box.input_alpha.input.Enable = 'on';
            window.akust_model_param.bassreflex_box.input_QgvG.input.Enable = 'on';
            window.akust_model_param.bassreflex_box.input_VvG.input.Enable = 'on';
            window.akust_model_param.bassreflex_box.input_l.input.Enable = 'on';            
        end
    end

    function filter_select_vG(hObject, ~)
        value = get(hObject,'Value');
        
        if (value == 1)
            window.akust_model_param.bassreflex_box.input_k.text_1.Visible = 'off';
            window.akust_model_param.bassreflex_box.input_k.input.Visible = 'off';
            [param.vG.a1,param.vG.a2,param.vG.a3] = Bessel_coeff_calc_vG();
        end
        
        if (value == 2)
            window.akust_model_param.bassreflex_box.input_k.text_1.Visible = 'off';
            window.akust_model_param.bassreflex_box.input_k.input.Visible = 'off';
            [param.vG.a1,param.vG.a2,param.vG.a3] = Butterworth_coeff_calc_vG();
        end
        
        if (value == 3)            
            window.akust_model_param.bassreflex_box.input_k.text_1.Visible = 'on';
            window.akust_model_param.bassreflex_box.input_k.input.Visible = 'on';
            [param.vG.a1,param.vG.a2,param.vG.a3] = Chebyshev_coeff_calc_vG(param.vG.k);
        end

        recalc_param;
        update_ui;
        
    end

    function k_changed_vG(hObject, ~)
        param.vG.k = str2double(get(hObject,'String'));
        [param.vG.a1,param.vG.a2,param.vG.a3] = Chebyshev_coeff_calc_vG(param.vG.k);
        recalc_param;
        update_ui;
    end

    function h_changed(hObject, ~)
        param.vG.h = str2double(get(hObject,'String'));
        param.vG.Th = h_calc_inv(param.vG.h,param.uS.TuS);
        recalc_param;
        update_ui;
    end

    function alpha_vG_changed(hObject, ~)
        param.vG.alpha = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function QgvG_changed(hObject, ~)
        param.vG.QgvG = str2double(get(hObject,'String'));
        recalc_param;
        update_ui;
    end

    function VvG_changed(hObject, ~)
        param.vG.VvG = (1e-3)*str2double(get(hObject,'String'));
        param.vG.alpha = VvG_calc_inv(param.vG.VvG,param.VaeMa);
        recalc_param;
        update_ui;
    end

    function r_changed(hObject, ~)
        param.vG.r = (1e-2)*str2double(get(hObject,'String'));
        if (param.vG.charact_opt == 0)
            param.vG.magBr = l_Br_calc_inv(param.vG.r,param.vG.l,param.vG.lk_1,param.vG.lk_2);
            param.vG.Th = magBr_calc_inv(param.vG.VvG,param.vG.magBr);
            param.vG.h = h_calc(param.vG.Th,param.uS.TuS);
        end
        recalc_param;
        update_ui;
    end

    function l_changed(hObject, ~)
        param.vG.l = (1e-2)*str2double(get(hObject,'String'));
        param.vG.magBr = l_Br_calc_inv(param.vG.r,param.vG.l,param.vG.lk_1,param.vG.lk_2);
        param.vG.Th = magBr_calc_inv(param.vG.VvG,param.vG.magBr);
        param.vG.h = h_calc(param.vG.Th,param.uS.TuS);
        recalc_param;
        update_ui;
    end

    function lk_1_changed(hObject, ~)
        param.vG.lk_1 = str2double(get(hObject,'String'));
        if (param.vG.charact_opt == 0)
            param.vG.magBr = l_Br_calc_inv(param.vG.r,param.vG.l,param.vG.lk_1,param.vG.lk_2);
            param.vG.Th = magBr_calc_inv(param.vG.VvG,param.vG.magBr);
            param.vG.h = h_calc(param.vG.Th,param.uS.TuS);
        end
        recalc_param;
        update_ui;
    end

    function lk_2_changed(hObject, ~)
        param.vG.lk_2 = str2double(get(hObject,'String'));
        if (param.vG.charact_opt == 0)
            param.vG.magBr = l_Br_calc_inv(param.vG.r,param.vG.l,param.vG.lk_1,param.vG.lk_2);
            param.vG.Th = magBr_calc_inv(param.vG.VvG,param.vG.magBr);
            param.vG.h = h_calc(param.vG.Th,param.uS.TuS);
        end
        recalc_param;
        update_ui;
    end

    % plot settings
    function diagram_scaling_select(hObject, ~)
        name = hObject.Parent.Tag;
        value = hObject.Value;
        
        if(value == 2)
            if(strcmp(name, 'imp'))
                xlim = window.tabs.diagram_imp_amp.XLim;
                window.tabs.diagram_settings_imp.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_imp.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_imp_amp.YLim;
                window.tabs.diagram_settings_imp.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_imp.input_ymax_1.String = convert_engineering_format(ylim(2));
                ylim = window.tabs.diagram_imp_pha.YLim;
                window.tabs.diagram_settings_imp.input_ymin_2.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_imp.input_ymax_2.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_imp.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'x'))
                xlim = window.tabs.diagram_x.XLim;
                window.tabs.diagram_settings_x.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_x.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_x.YLim;
                window.tabs.diagram_settings_x.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_x.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_x.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'G_Pa'))
                xlim = window.tabs.diagram_G_Pa.XLim;
                window.tabs.diagram_settings_G_Pa.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_G_Pa.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_G_Pa.YLim;
                window.tabs.diagram_settings_G_Pa.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_G_Pa.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_G_Pa.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'p'))
                xlim = window.tabs.diagram_p.XLim;
                window.tabs.diagram_settings_p.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_p.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_p.YLim;
                window.tabs.diagram_settings_p.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_p.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_p.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'n'))
                xlim = window.tabs.diagram_n.XLim;
                window.tabs.diagram_settings_n.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_n.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_n.YLim;
                window.tabs.diagram_settings_n.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_n.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_n.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'G_norm'))
                xlim = window.tabs.diagram_G_norm_amp.XLim;
                window.tabs.diagram_settings_G_norm.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_G_norm.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_G_norm_amp.YLim;
                window.tabs.diagram_settings_G_norm.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_G_norm.input_ymax_1.String = convert_engineering_format(ylim(2));
                ylim = window.tabs.diagram_G_norm_pha.YLim;
                window.tabs.diagram_settings_G_norm.input_ymin_2.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_G_norm.input_ymax_2.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_G_norm.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'g'))
                xlim = window.tabs.diagram_g.XLim;
                window.tabs.diagram_settings_g.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_g.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_g.YLim;
                window.tabs.diagram_settings_g.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_g.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_g.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'h'))
                xlim = window.tabs.diagram_h.XLim;
                window.tabs.diagram_settings_h.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_h.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_h.YLim;
                window.tabs.diagram_settings_h.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_h.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_h.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'group'))
                xlim = window.tabs.diagram_group.XLim;
                window.tabs.diagram_settings_group.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_group.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_group.YLim;
                window.tabs.diagram_settings_group.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_group.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_group.panel_xyz.Visible = 'on';
            end
            if(strcmp(name, 'v'))
                xlim = window.tabs.diagram_v.XLim;
                window.tabs.diagram_settings_v.input_xmin.String = convert_engineering_format(xlim(1));
                window.tabs.diagram_settings_v.input_xmax.String = convert_engineering_format(xlim(2));
                ylim = window.tabs.diagram_v.YLim;
                window.tabs.diagram_settings_v.input_ymin_1.String = convert_engineering_format(ylim(1));
                window.tabs.diagram_settings_v.input_ymax_1.String = convert_engineering_format(ylim(2));
                window.tabs.diagram_settings_v.panel_xyz.Visible = 'on';
            end
        else
            if(strcmp(name, 'imp'))
                window.tabs.diagram_settings_imp.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'x'))
                window.tabs.diagram_settings_x.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'G_Pa'))
                window.tabs.diagram_settings_G_Pa.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'p'))
                window.tabs.diagram_settings_p.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'n'))
                window.tabs.diagram_settings_n.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'G_norm'))
                window.tabs.diagram_settings_G_norm.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'g'))
                window.tabs.diagram_settings_g.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'h'))
                window.tabs.diagram_settings_h.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'group'))
                window.tabs.diagram_settings_group.panel_xyz.Visible = 'off';
            end
            if(strcmp(name, 'v'))
                window.tabs.diagram_settings_v.panel_xyz.Visible = 'off';
            end
        end
        
        update_ui;
    end

    function axes_scale_value_changed(hObject, ~)        
        update_ui;
    end

    function compare_curves_changed(hObject, ~) 
        update_ui;
    end

    function export_curves_changed(hObject, ~)  
        update_ui;
    end

end