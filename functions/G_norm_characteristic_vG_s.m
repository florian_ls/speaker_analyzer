%-----------------------------------------------------------------------------------------
% G_norm_characteristic_vG_s.m - function to calculate norm transfer function laplace of 
% bassreflex box
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%-----------------------------------------------------------------------------------------

function y = G_norm_characteristic_vG_s(Th, TuS, a1, a2, a3)
    s = tf('s');
    To = sqrt(Th*TuS);
    y = s^4*To^4 /...
        (s^4*To^4 + a1*s^3*To^3 + a2*s^2*To^2 + a3*s*To +1);
end