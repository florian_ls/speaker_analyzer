%--------------------------------------------------------------------------
% matrix_Zbr_calc.m - function to calculate akustic matrix of bassreflex
% tube
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function A = matrix_Zbr_calc(r, l, Zbr, w)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    Abr = r^2*pi;
    k = w/c;
    Z1 = Zbr*Abr;
    Za_1 = rho*c/Abr * (Z1/(rho*c) + 1i.*tan(k*l))./(1 + 1i.*Z1/(rho*c).*tan(k*l));
    Za = Za_1 + Zbr;
    
    a11 = 1;
    a12 = 0;
    a21 = 1/Za;
    a22 = 1;    
    A = [a11 a12; a21 a22];
end
