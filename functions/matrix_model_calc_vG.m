%--------------------------------------------------------------------------
% matrix_model_calc_vG.m - function to calculate advanced matrix model
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function [Zges,Zm,qm,qbr,qa] = matrix_model_calc_vG(Ug, Rg, Rs, Ls, Bl, RmMa, smMa, mmMk, Am, alpha, Ql, Th, r, l, w)
    Zges = zeros(1,length(w));
    Zm = zeros(1,length(w));
    qm = zeros(1,length(w));
    qbr = zeros(1,length(w));
    qa = zeros(1,length(w));
    
    % calculate for all frequencies
    for i = 1:length(w)
        Ag = matrix_Rg_calc(Rg);
        A1 = matrix_electr_calc(Rs, Ls, w(i));
        A2 = matrix_electromech_calc(Bl);
        A3 = matrix_mech_calc(RmMa, smMa, mmMk, w(i));
        A4 = matrix_mechanoakust_calc(Am);
        Zm(i) = Za_calc(Am, w(i));
        A5 = matrix_Za_2_calc(Zm(i));
        A6 = matrix_gG_2_calc(alpha, Am, smMa, w(i));
        A7 = matrix_Rl_calc(alpha, smMa, Ql, Th, Am);
        Zbr = Za_calc(r^2*pi, w(i));
        A8 = matrix_Zbr_calc(r, l, Zbr, w(i));
        
        Abr = A6*A7*A8;
        Asp = A1*A2*A3*A4*A5*Abr;
        Ages = Ag*Asp;

        Zges(i) = Asp(1,1)/Asp(2,1); 
        p = Ug/Ages(1,1);
        qbr(i) = p/(A8(1,1)/A8(2,1));
        qm(i) = p/(Abr(1,1)/Abr(2,1));
        Za = A6(1,1)/A6(2,1);
        qa(i) = p/Za;
    end
end
