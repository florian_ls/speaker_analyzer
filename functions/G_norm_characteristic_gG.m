%-----------------------------------------------------------------------------------------
% G_norm_characteristic_gG.m - function to calculate norm transfer characteristic function
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%-----------------------------------------------------------------------------------------

function y = G_norm_characteristic_gG(w, TgG, a1)
    s = 1i.*w;
    y = s.^2.*TgG^2 ./ (1+s.*a1*TgG+s.^2.*TgG^2);
end