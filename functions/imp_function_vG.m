%----------------------------------------------------------------------------
% imp_function_vG.m - function to calculate impedance function bassreflex box
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%----------------------------------------------------------------------------

function y = imp_function_vG(w, TuS, Th, QmMk, Ql, alpha, RmguS, Bl, Rs)
    s = 1i.*w;
    ReguS = Bl^2 / RmguS;
    y = Rs + ReguS .* s.*(TuS/QmMk).*(1+s.*Th/Ql+s.^2*Th^2) ./...
        (s.^4*Th^2*TuS^2 + s.^3*(Th^2*TuS/QmMk+Th*TuS^2/Ql) + s.^2*(Th^2*(1+alpha)+Th*TuS/(Ql*QmMk)+TuS^2) + s.*(Th/Ql+TuS/QmMk) +1);
end