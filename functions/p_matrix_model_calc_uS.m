%--------------------------------------------------------------------------
% p_matrix_model_calc_uS.m - function to calculate acoustic pressure in 
% advanced matrix model
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function p = p_matrix_model_calc_uS(q, r, theta, Am, w)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    
    theta_rad = theta*pi/180;
    rm = sqrt(Am/pi);
    k = w/c;

    if (theta_rad == 0)
        gamma_ko = 1;
    else
        gamma_ko = 2.*besselj(1,k.*rm.*sin(theta_rad))./(k.*rm.*sin(theta_rad));
    end
    
    p = q.*gamma_ko.*1i.*k.*rho.*c .* exp(-1i.*k.*r)./(2*r*pi);
end
