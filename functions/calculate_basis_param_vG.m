%-------------------------------------------------------------------------------
% calculate_basis_param_vG.m - function to calculate [h,Th,QgvG,alpha] from a1,a2,a3,Ql,TuS

% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------------

function [h,Th,QgvG,alpha] = calculate_basis_param_vG(a1,a2,a3,Ql,TuS)

% calculate h
p0 = -1;
p1 = Ql*a3;
p2 = 0;
p3 = (-1)*Ql*a1;
p4 = 1;

y1 = roots([p4 p3 p2 p1 p0]);   % calculat results of polynom 4. order

flags = [1 1 1 1];              % flag of each value: set 0 if it is not positiv and real

for i=1:4
    if (imag(y1(i)) > 0 || y1(i) < 0)
        flags(i) = 0;
    end
end

h_all = y1.^2;


% calculate QgvG
y1 = Ql ./ (Ql.*a1.*h_all.^(1/2) - h_all);

for i=1:4
    if (imag(y1(i)) > 0 || y1(i) < 0)
        flags(i) = 0;
    end
end

QgvG_all = y1;


% calculate alpha
y1 = a2.*h_all - h_all./(Ql.*QgvG_all) - h_all.^2 - 1;

for i=1:4
    if (imag(y1(i)) > 0 || y1(i) < 0)
        flags(i) = 0;
    end
end

alpha_all = y1;


% choose the only posible values
[value, index] = max(flags);

h = h_all(index);
QgvG = QgvG_all(index);
alpha = alpha_all(index);

Th = Th_calc(h, TuS);

end