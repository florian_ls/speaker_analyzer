%--------------------------------------------------------------------------
% G_norm_matrix_model.m - function to calculate norm transfer function in
% advanced matrix model
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = G_norm_matrix_model(Ug, Rg, Rs, Ls, Bl, RmMa, smMa, mmMk, mmguS, Am)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    r = 1;

    Ag = matrix_Rg_calc(Rg);
    A1 = matrix_electr_calc_s(Rs, Ls);
    A2 = matrix_electromech_calc(Bl);
    A3 = matrix_mech_calc_s(RmMa, smMa, mmMk);
    A4 = matrix_mechanoakust_calc(Am);
    Za = Za_calc_s(Am);
    A5 = matrix_Za_calc(Za);
    Asp = A1*A2*A3*A4*A5;
    Ages = Ag*Asp;

    s = tf('s');
    p = Ug/Ages(1,1);
    q = p/Za;
    pa = s*q*rho/(2*r*pi);
    y = 1/(Ug * rho/(2*pi*r) * Bl*Am/((Rg+Rs)*mmguS)) * pa;
end