%--------------------------------------------------------------------------
% matrix_model_calc.m - function to calculate advanced matrix model
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function [Zges,Za,p,q] = matrix_model_calc(Ug, Rg, Rs, Ls, Bl, RmMa, smMa, mmMk, Am, w)
    Zges = zeros(1,length(w));
    Za = zeros(1,length(w));
    q = zeros(1,length(w));
    p = zeros(1,length(w));
    
    % calculate for all frequencies
    for i = 1:length(w)
        Ag = matrix_Rg_calc(Rg);
        A1 = matrix_electr_calc(Rs, Ls, w(i));
        A2 = matrix_electromech_calc(Bl);
        A3 = matrix_mech_calc(RmMa, smMa, mmMk, w(i));
        A4 = matrix_mechanoakust_calc(Am);
        Za(i) = Za_calc(Am, w(i));
        A5 = matrix_Za_calc(Za(i));
        Asp = A1*A2*A3*A4*A5;
        Ages = Ag*Asp;

        Zges(i) = Asp(1,1)/Asp(2,1); 
        p(i) = Ug/Ages(1,1);
        q(i) = p(i)/Za(i);
    end
end
