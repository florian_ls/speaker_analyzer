%--------------------------------------------------------------------------
% mauS_calc.m - function to calculate nauS from Am
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = mauS_calc(Am)

c = 343.2;      % sound velocity in air [m/s] at 20°C
rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C

rm = sqrt(Am/pi);
y = 8*rho / (3*pi^2*rm);

end