%--------------------------------------------------------------------------
% Ug_calc_uS_inv.m - function to calculate sound pressure from ug
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function p_dB = Ug_calc_uS_inv(Ug, r, Bl, Am, Rg, Rs, mmguS)
    rho = 1.2041;           % sound density in air [kg/m^3] at 20°C

    p  = Ug * rho/(2*pi*r) * (Bl*Am)/((Rg+Rs)*mmguS);
    p_dB = 20*log10(p/(20e-6));
end