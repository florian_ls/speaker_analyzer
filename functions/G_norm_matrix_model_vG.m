%--------------------------------------------------------------------------
% G_norm_matrix_model_vG.m - function to calculate norm transfer function
% advanced matrix model of bassreflex box
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function G_norm = G_norm_matrix_model_vG(Ug, Rg, Rs, Ls, Bl, RmMa, smMa, mmMk, Am, alpha, mmguS, Ql, Th, r, l)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    d = 1;
    
    Ag = matrix_Rg_calc(Rg);
    A1 = matrix_electr_calc_s(Rs, Ls);
    A2 = matrix_electromech_calc(Bl);
    A3 = matrix_mech_calc_s(RmMa, smMa, mmMk);
    A4 = matrix_mechanoakust_calc(Am);
    Zm = Za_calc_s(Am);
    A5 = matrix_Za_2_calc(Zm);
    A6 = matrix_gG_2_calc_s(alpha, Am, smMa);
    A7 = matrix_Rl_calc(alpha, smMa, Ql, Th, Am);
    Zbr = Za_calc_s(r^2*pi);
    A8 = matrix_Zbr_calc_s(r, l, Zbr);

    Abr = A6*A7*A8;
    Asp = A1*A2*A3*A4*A5*Abr;
    Ages = Ag*Asp;

    s = tf('s');
%         Zges = Asp(1,1)/Asp(2,1); 
    p = Ug/Ages(1,1);
%         qbr = p/(A8(1,1)/A8(2,1));
%         qm = p/(Abr(1,1)/Abr(2,1));
    Za = A6(1,1)/A6(2,1);
    qa = p/Za;
    pa = s*qa*rho/(2*d*pi);
    G_norm = 1/(Ug * rho/(2*pi*d) * Bl*Am/((Rg+Rs)*mmguS)) * pa;
end
