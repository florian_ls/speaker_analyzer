%--------------------------------------------------------------------------
% alpha_gG_calc.m - function to calculate alpha_gG_calc from QggG, QmMk, 
% QeMk, QmVgG, mgG 
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = alpha_gG_calc(QggG, QmMk, QeMk, QmVgG, mgG)

y = ((1/QmMk + 1/QeMk)/(1/QggG - 1/QmVgG))^2 * 1/mgG - 1;

end
