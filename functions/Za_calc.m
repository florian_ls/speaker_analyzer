%--------------------------------------------------------------------------
% Za_calc.m - function to calculate akustic impedanz from Am, w
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function Za = Za_calc(Am, w)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C

    k = w/c;
    rm = sqrt(Am/pi);

    Rs_norm = 1-besselj(1,2*k*rm)./(k*rm);
    Xs_norm = struveH1(2*k*rm)./(k*rm);

    Zs = rho*c * (Rs_norm + 1i*Xs_norm);
    Za = Zs/Am;
end
