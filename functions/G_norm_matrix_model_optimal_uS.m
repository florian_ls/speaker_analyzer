%--------------------------------------------------------------------------
% G_norm_matrix_model_optimal_uS.m - function to calculate norm transfer 
% optimal function in advanced matrix model
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = G_norm_matrix_model_optimal_uS(Ug, Rg, Rs, Bl, mmguS, Am, pa, w)
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    r = 1;
    k = w/c;

    y = 1/(Ug * rho/(2*pi*r) * Bl*Am/((Rg+Rs)*mmguS)) * pa .* exp(+1i.*k.*r);
end