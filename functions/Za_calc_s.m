%--------------------------------------------------------------------------
% Za_calc_s.m - function to calculate akustic impedanz laplace from Am
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function Za = Za_calc_s(Am)
    s = tf('s');
    c = 343.2;      % sound velocity in air [m/s] at 20°C
    rho = 1.2041;   % density of air 1,2041kg/m^3 at 20°C
    rm = sqrt(Am/pi);

    Ra_1 = 0.1404 * rho*c/rm^2;
    Ra_2 = rho*c/(pi*rm^2);
    sa = 0.53 * rho*c^2/(pi*rm^3);
    ma = 8/3 * rho/(pi^2*rm);

    Za = s*ma * (1 + s/sa*Ra_1*Ra_2/(Ra_1+Ra_2)) / ...
        (1 + s/sa*(Ra_1*Ra_2/(Ra_1+Ra_2)+ma*sa/(Ra_1+Ra_2)) + s^2*ma/sa*Ra_1/(Ra_1+Ra_2));
end
