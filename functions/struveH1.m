%--------------------------------------------------------------------------
% struveH1.m - calculate struve function approximation based on the paper
% https://asa.scitation.org/doi/abs/10.1121/1.1564019
% This file is part of Speaker Analyzer.
% 
% Copyright 2018 Florian Loacker-Schoech
%
% Speaker Analyzer is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% Speaker Analyzer is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with Speaker Analyzer. If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

function y = struveH1(z)
    y = 2/pi - besselj(0,z) + (16/pi-5).*sin(z)./z + (12-36/pi).*(1-cos(z))./z.^2;
end
